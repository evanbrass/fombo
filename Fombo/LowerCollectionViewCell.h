//
//  LowerCollectionViewCell.h
//  Fombo
//
//  Created by Evan Brass on 4/25/15.
//  Copyright (c) 2015 Evan Brass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FontCollectionViewCell.h"

@interface LowerCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@end
