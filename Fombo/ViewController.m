//
//  ViewController.m
//  Fombo
//
//  Created by Evan Brass on 4/24/15.
//  Copyright (c) 2015 Evan Brass. All rights reserved.
//

#import "ViewController.h"
#import "FontCollectionViewCell.h"
#import "LowerCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "FontCurator.h"

#define SCREENSHOT_MENU_HEIGHT 120.0
#define CAMERA_SHUTTER_SOUND 1108
#define SAVE_SOUND 1118
#define TEXT_PERCENTAGE 0.65

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSArray *fontList;
@property (nonatomic, strong) UIImageView *screenShotImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *screenShotBottomConstraint;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UICollectionView *upperCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *lowerCollectionView;
@property (weak, nonatomic) IBOutlet UIView *screenShotMenuView;

@end

@implementation ViewController

# pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Setup font array
    [self setupCollectionViews:@[self.upperCollectionView, self.lowerCollectionView]];
//    self.fontList = [UIFont familyNames];
    
    // Hide screenshot menu
    self.screenShotBottomConstraint.constant = -SCREENSHOT_MENU_HEIGHT;
}

- (void)viewDidAppear:(BOOL)animated {
    // Get rid of some of the lame system fonts and alphabetize
    FontCurator *curator = [[FontCurator alloc] init];
    
    self.fontList = [curator sortedFonts];
    self.fontList = [self.fontList sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self.upperCollectionView reloadData];
    [self.lowerCollectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

# pragma mark - Setup 

- (void)setupCollectionViews:(NSArray *)collectionViewArray {
    for (UICollectionView *collectionView in collectionViewArray) {
        collectionView.delegate = self;
        collectionView.dataSource = self;
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [flowLayout setMinimumInteritemSpacing:0.0f];
        [flowLayout setMinimumLineSpacing:0.0f];
        [collectionView setCollectionViewLayout: flowLayout];
        
        [collectionView setPagingEnabled:YES];
        collectionView.showsHorizontalScrollIndicator = NO;
    }
    [self.upperCollectionView registerClass:[FontCollectionViewCell class] forCellWithReuseIdentifier:@"CustomCell"];
    [self.lowerCollectionView registerClass:[LowerCollectionViewCell class] forCellWithReuseIdentifier:@"LowerCellNew"];
}

# pragma mark - Collection View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.fontList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FontCollectionViewCell *cell;
    
    if (collectionView == self.upperCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CustomCell" forIndexPath:indexPath];
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LowerCellNew" forIndexPath:indexPath];
    }
    
    cell.label.text = [self.fontList objectAtIndex:indexPath.row];
    cell.label.font = [UIFont fontWithName:[self.fontList objectAtIndex:indexPath.row]
                                      size:cell.label.frame.size.height * TEXT_PERCENTAGE];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.frame.size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FontCollectionViewCell *cell = (FontCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSArray *fontStyles = [UIFont fontNamesForFamilyName:cell.label.font.familyName];
    
    if (![fontStyles containsObject:cell.label.font.fontName]) {
        cell.label.font = [UIFont fontWithName:[fontStyles objectAtIndex:0] size:cell.label.frame.size.height * TEXT_PERCENTAGE];
    } else {
        int index = (int)[fontStyles indexOfObject:cell.label.font.fontName];
        int nextIndex = (index + 1) % [fontStyles count];
        cell.label.font =[UIFont fontWithName:[fontStyles objectAtIndex:nextIndex]
                                         size:cell.label.frame.size.height * TEXT_PERCENTAGE];
    }
}

#pragma mark - IBActions

- (IBAction)screenShotButtonAction:(id)sender
{
    self.shareButton.hidden = YES;
    UIImage *screenShot = [self imageWithView:self.view];
    [self takeScreenShot:screenShot];
}

- (IBAction)cancelButtonAction:(id)sender
{
    [self dismissScreenShotMenu];
}

- (IBAction)saveButtonAction:(id)sender
{
    if (self.screenShotImageView.image) {
        UIImageWriteToSavedPhotosAlbum(self.screenShotImageView.image, self,
                                       @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
}

- (IBAction)shareButtonAction:(id)sender
{
    UIActivityViewController *avc = [[UIActivityViewController alloc]
                                     initWithActivityItems:@[self.screenShotImageView.image]
                                     applicationActivities:nil];
    
    [self presentViewController:avc animated:YES completion:^{
        [self dismissScreenShotMenu];
    }];
}


#pragma mark - Helper

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    AudioServicesPlaySystemSound(SAVE_SOUND);
    [self dismissScreenShotMenu];
}

- (void)dismissScreenShotMenu
{
    self.shareButton.hidden = NO;
    [self.screenShotImageView removeFromSuperview];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.screenShotBottomConstraint.constant = -SCREENSHOT_MENU_HEIGHT;
        [self.screenShotMenuView layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)takeScreenShot:(UIImage *)screenShot
{
    // Make camera sound
    AudioServicesPlaySystemSound(CAMERA_SHUTTER_SOUND);
    
    // Add screenshot below screenshot menu
    self.screenShotImageView = [[UIImageView alloc] initWithImage:screenShot];
    [self.view insertSubview:self.screenShotImageView belowSubview:self.screenShotMenuView];
    
    // Flash white
    UIView *flashView = [[UIView alloc] initWithFrame:self.view.frame];
    flashView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:flashView];
    
    // Fade flash view
    [UIView animateWithDuration:1.0 animations:^{
        flashView.alpha = 0;
            self.screenShotBottomConstraint.constant = 0;
            [self.screenShotMenuView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [flashView removeFromSuperview];
    }];
}

- (UIImage *) imageWithView:(UIView *)view
{
    // Returns image from view
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
