//
//  FontCurator.h
//  Fombo
//
//  Created by Evan Brass on 4/25/15.
//  Copyright (c) 2015 Evan Brass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FontCurator : NSObject

- (NSArray *)sortedFonts;

@end
