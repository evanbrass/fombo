//
//  FontCollectionViewCell.m
//  Fombo
//
//  Created by Evan Brass on 4/24/15.
//  Copyright (c) 2015 Evan Brass. All rights reserved.
//

#import "FontCollectionViewCell.h"

@implementation FontCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"FontCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
        self.label.adjustsFontSizeToFitWidth = YES;
    }
    
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

@end
