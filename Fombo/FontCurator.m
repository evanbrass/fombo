//
//  FontCurator.m
//  Fombo
//
//  Created by Evan Brass on 4/25/15.
//  Copyright (c) 2015 Evan Brass. All rights reserved.
//

#import "FontCurator.h"

@implementation FontCurator

- (NSArray *)sortedFonts {
    // Ignore some of the lame system fonts
    NSSet *ignoreList = [[NSSet alloc] initWithObjects:@"Academy Engraved LET",
                         @"Apple Color Emoji",
                         @"Arial Rounded MT Bold",
                         @"Bodoni 72 Smallcaps",
                         @"Bodoni Ornaments",
                         @"Bradley Hand",
                         @"Chalkduster",
                         @"Damascus",
                         @"Devanagari Sangam MN",
                         @"DIN Alternate",
                         @"DIN Condensed",
                         @"Farah",
                         @"Hiragino Kaku Gothic ProN",
                         @"Khmer Sangam MN",
                         @"Lao Sangam MN",
                         @"Mishafi",
                         @"Party LET",
                         @"Savoye LET",
                         @"Sinhala Sangam MN",
                         @"Symbol",
                         @"Zapf Dingbats",
                         @"Zapfino",
                         nil];
    NSMutableArray *fonts = [@[] mutableCopy];
    
    for (NSString *fontName in [UIFont familyNames]) {
        if (![ignoreList containsObject:fontName]) {
            [fonts addObject:fontName];
        }
    }
    
    NSArray *alphabetizedFonts = [fonts sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return alphabetizedFonts;
}

@end
