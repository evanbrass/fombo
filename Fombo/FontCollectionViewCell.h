//
//  FontCollectionViewCell.h
//  Fombo
//
//  Created by Evan Brass on 4/24/15.
//  Copyright (c) 2015 Evan Brass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FontCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@end
